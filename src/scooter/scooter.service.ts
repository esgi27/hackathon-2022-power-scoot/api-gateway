import { Scooter } from './scooter.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class ScooterService {
    constructor(
        @InjectRepository(Scooter)
        private readonly scooterRepository: Repository<Scooter>,
    ) {}

    async findAll(): Promise<Scooter[]> {
        return await this.scooterRepository.find();
    }

    async findById(id: number): Promise<Scooter> {
        return await this.scooterRepository.findOne(id);
    }

    async findByScootId(sid: string){
        const scooters = await this.scooterRepository.find({where: {scoot_id: sid}})        
        return scooters[0];
    }

    async findLast(): Promise<Scooter> {
        const scooters = await this.findAll();
    
        return scooters[scooters.length - 1]
    }

    // async findOne(scooterId: string): Promise<Scooter> {
    //     return this.scooterRepository.findOne({
    //         scooterId,
    //     });
    // }
}
