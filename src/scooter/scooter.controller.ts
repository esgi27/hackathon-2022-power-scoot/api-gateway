import {
    Controller,
    Get,
    Param,
} from '@nestjs/common';

import { ScooterService } from './scooter.service';
import { Scooter } from './scooter.entity';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('scooters')
@Controller('scooters')
export class ScooterController {
    constructor(private readonly scooterService: ScooterService) {}

    @Get()
    async findAll(): Promise<Scooter[]> {
        return await this.scooterService.findAll();
    }

    @Get('/:scootId')
    async findScooterByScootId(@Param('scootId') scootId: string): Promise<Scooter> {
        const scooter = await this.scooterService.findByScootId(scootId);

        return scooter;
    }

    // @Get('/:scooterId')
    // async findAScooter(@Param() params): Promise<Scooter> {
    //     const scooter = await this.scooterService.findOne(params.scooterId);
    //     if (!scooter) {
    //         throw new HttpException(
    //             {
    //                 status: HttpStatus.NOT_FOUND,
    //                 error: `Scooter not found with scoot_id = '${params.scooterId}'`,
    //             },
    //             HttpStatus.NOT_FOUND,
    //         );
    //     }
    //     return scooter;
    // }
}
