import { Column, CreateDateColumn, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { ScootHistory } from '../scooter-history/scooter-history.entity';

@Entity('scooter')
export class Scooter {
    @PrimaryColumn({ primary: true })
    id: number;

    @Column({ name: 'scoot_id' })
    scoot_id: string;
    
    @CreateDateColumn()
    created_at: Date;

    @OneToMany(() => ScootHistory, scootHistory => scootHistory.scooter)
    scootHistories: ScootHistory[]
}
