// import { Test, TestingModule } from '@nestjs/testing';
// import { ScooterController } from './scooter.controller';
// import { ScooterService } from './scooter.service';
// import { Scooter } from './scooter.entity';

// describe('ScooterController', () => {
//     let scooterController: ScooterController;
//     let scooterService: ScooterService;
//     const scooters: Scooter[] = [
//         {
//             id: 2,
//             scooterId: 'ID_1',
//             batteryLevel: 30,
//         },
//         {
//             id: 1,
//             scooterId: 'ID_2',
//             batteryLevel: 50,
//         },
//     ];

//     beforeEach(async () => {
//         const module: TestingModule = await Test.createTestingModule({
//             controllers: [ScooterController],
//             providers: [
//                 {
//                     provide: ScooterService,
//                     useValue: {
//                         findAll: jest.fn(() => scooters),
//                         findOne: jest.fn(() => scooters[0]),
//                     },
//                 },
//             ],
//         }).compile();

//         scooterController = module.get(ScooterController);
//         scooterService = module.get(ScooterService);
//     });

//     it('should be defined', () => {
//         expect(scooterController).toBeDefined();
//     });

//     it('should be defined', () => {
//         expect(scooterService).toBeDefined();
//     });

//     describe('findAll', () => {
//         it('should return an array of scooters', async () => {
//             expect(await scooterController.findAll()).toBe(scooters);
//         });
//     });

//     describe('findOne', () => {
//         it('should return only one scooter', async () => {
//             // expect(
//             //     await scooterController.findAScooter({ scooterId: 'ID_1' }),
//             // ).toBe(scooters[0]);
//         });
//     });
// });
