import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ScooterHistoryController } from "./scooter-history.controller";
import { ScooterHistoryService } from "./scooter-history.service";
import { ScootHistory } from './scooter-history.entity';
import { ScooterModule } from "src/scooter/scooter.module";

@Module({
    imports: [TypeOrmModule.forFeature([ScootHistory]), ScooterModule],
    controllers: [ScooterHistoryController],
    exports: [ScooterHistoryService],
    providers: [ScooterHistoryService,],
})
export class ScooterHistoryModule {}