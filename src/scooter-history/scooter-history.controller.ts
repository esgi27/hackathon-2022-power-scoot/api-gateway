import { Controller, Get, Param } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ScooterHistoryService } from './scooter-history.service';
import { ScootHistory } from './scooter-history.entity';


@ApiTags('scooter history')
@Controller('scootHistory')
export class ScooterHistoryController {
	constructor(private readonly scooterHistoryService: ScooterHistoryService) {}

    @Get()
    async findAll(): Promise<ScootHistory[]> {
        return await this.scooterHistoryService.findAll();
    }

    @Get('/:id/last-history')
    async findLast(@Param('id') id: number): Promise<ScootHistory> {
        return await this.scooterHistoryService.findScootLastHistory(id);
    }

    @Get(':id')
    async findHistoriesByScootId(@Param('id') id: number): Promise<ScootHistory[]> {
        return await this.scooterHistoryService.findHistoryByScootId(id);
    }
}