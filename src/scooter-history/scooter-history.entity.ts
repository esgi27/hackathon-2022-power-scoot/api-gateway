import { type } from "os";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryColumn, TableForeignKey } from "typeorm";
import { Scooter } from '../scooter/scooter.entity';


@Entity('scoot_history')
export class ScootHistory{
	@PrimaryColumn({ primary: true })
    id: number;
	
	@Column()
	battery_level: number;
	
	@Column()
	longitude: number;
	
	@Column()
	latitude: number;
	
	@CreateDateColumn()
	created_at: Date;

	@ManyToOne(() => Scooter, scooter => scooter.scootHistories)
	scooter: Scooter;
}