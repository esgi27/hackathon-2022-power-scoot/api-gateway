import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ScootHistory } from './scooter-history.entity';
import { ScooterService } from '../scooter/scooter.service';


@Injectable() 
export class ScooterHistoryService {
	constructor(
        @InjectRepository(ScootHistory)
        private readonly scooterHistoryRepository: Repository<ScootHistory>,
		private readonly scooterService: ScooterService,
    ) {}

    async findAll(): Promise<ScootHistory[]> {
        return await this.scooterHistoryRepository.find();
    }
	
	async findHistoryByScootId(id: number): Promise<ScootHistory[]> {
		const foundScooter = await this.scooterService.findById(id);
		if(foundScooter){
			return await this.scooterHistoryRepository.find({where: { scooter: foundScooter}})
		}
		return [];
	}

	async findScootLastHistory(id: number): Promise<ScootHistory> {
		const scooterHistories = await this.findHistoryByScootId(id);
		return scooterHistories[scooterHistories.length - 1];
	}
}