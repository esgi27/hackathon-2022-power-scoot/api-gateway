import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScooterModule } from './scooter/scooter.module';
import { ScooterController } from './scooter/scooter.controller';
import { config } from 'dotenv';
import { ScooterHistoryModule } from './scooter-history/scooter-history.module';

config();

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE,
            entities: ['dist/**/**.entity{.ts,.js}'],
            synchronize: false,
        }),
        ScooterModule,
        ScooterHistoryModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
